import React from 'react';
//import { Route, BrowserRouter as Router, Switch } from "react-router-dom";


import { ThemeProvider } from '@material-ui/styles';
import { createMuiTheme } from '@material-ui/core/styles';
import { StylesProvider } from '@material-ui/styles';


import Home from './components/home/Home'
import './App.css';


const theme = createMuiTheme({
 
  palette: {
    primary: {
      main: '#fed200',
      gray: '#d7d7d7',
      darkblack: '#000000',
      lightblack: '#5d511d',
     
    }
   
  }
});




const App = () => {
  
  return (
    
      <ThemeProvider theme={theme}>
          <Home/>
      </ThemeProvider>

  );
}

export default App;
