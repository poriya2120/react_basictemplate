import React, { Component } from 'react';
import { Container, CircularProgress } from '@material-ui/core';
import { withStyles } from '@material-ui/styles';
import { NavLink } from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';


const styles = theme => ({
    mainContainerBackGround: {
        background: '#FCF2FB',
    },
    mainContainer: {
        height: '100vh',
    },
    mainImage: {
        width: '100%',
    },
    signUpBox: {
        background: "#fff",
        boxShadow: " 0px 6px 10px rgb(0,0,0,16%)",
        borderRadius: 10
    },
    Bama: {
        fontWeight: 'bold',
        color: theme.palette.primary.dark,
        marginBottom: "2rem"
    },
    pleaseSignUp: {
        color: theme.palette.primary.dark,
    },
    buttonFrom: {
        marginTop: "1.5rem",
        letterSpacing: 1
    },
    signUpLink: {
        color: '#73A2DF',
        paddingLeft: ".4rem"
    },
    forgetLink: {
        color: '#424A71',
        textDecoration: 'none'
    },

});

class HomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            errors: '',
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.ui.errors) {
          this.setState({ errors: nextProps.ui.errors });
        }
    }

    HandleClickLogin = (event) => {
        event.preventDefault();
        // console.log(this.state.username, this.state.password);
        const userData = {
            usernameORemail: this.state.username,
            password: this.state.password
        }
        this.props.userLogin(userData,this.props.history);
    }


    handleChange = (event) => {
        // console.log(event.target.name, event.target.value);
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    // handleChange = (event) =>{
    //     this.setState
    // }
    

    render(){
        const {classes, ui: {loading} } = this.props;
        const { errors } = this.state;
        // console.log('err:',errors);
        return (
            <>
            <CssBaseline />
            <main className={classes.mainContainerBackGround} >
                <Container maxWidth="lg">
                    <Grid container className={classes.mainContainer} display="flex" direction="row" justify="space-around" alignItems="center">
                        <Grid item xs={11} sm md={5} lg={4} >
                            <Box className={classes.signUpBox} px={6} py={7} display="flex" flexDirection="column" justify="center" alignItems="center" >
                                <NavLink to="/" style={{ textDecoration: 'none' }}>
                                    <Typography className={classes.Bama} variant="h3" component="h1">
                                        باما
                                    </Typography>
                                </NavLink>
                               
                                <Box component="form" className={classes.form} my={3} display="flex" flexDirection="column">
                                    <TextField
                                        id="username"
                                        name="username"
                                        label="نام کاربری"
                                        placeholder="نام کاربری یا ایمیل خود را وارد کنید"
                                        className={classes.textField}
                                        margin="normal"
                                        // helperText="نام کاربری اشتباه است"
                                        error={errors ? true : false}
                                        variant="outlined"
                                        onChange={this.handleChange}
                                    />
                                    <TextField
                                        id="password"
                                        name="password"
                                        label="رمز عبور"
                                        placeholder="رمز عبور خود را وارد کنید"
                                        className={classes.textField}
                                        margin="normal"
                                        // helperText="رمزعبور اشتباه است"
                                        error={errors ? true : false}
                                        variant="outlined"
                                        onChange={this.handleChange}
                                    />
                                    {errors ==='INVALID_USERNAME_OR_PASSWORD' ? (
                                        <Typography variant="body2" component="p" style={{color: "red"}}>نام کاربری یا رمز عبور اشتباه است!</Typography>
                                    ): null}
                                    <Button disabled={loading} size="large" onClick={this.HandleClickLogin} variant="contained" className={classes.buttonFrom} color="primary">
                                        {loading && (
                                            <CircularProgress size={30} style={{ marginLeft:"10px"}}/>
                                        )}
                                    ورود
                                    </Button>
                                </Box>
                                <Typography variant="body2" component="p" >
                                    <a className={classes.forgetLink} href="/">رمز عبور خود را فراموش کرده اید؟</a>
                                </Typography>
                            </Box>
                        </Grid>
                        <Grid item xs={11} sm md={5} lg={7}>
                            <img className={classes.mainImage} src={loginBackground} alt="signUpImage" />
                        </Grid>
                    </Grid>
                </Container>
            </main>
            </>
        );
    }
}



export default HomePage
