import React from 'react';
import Carousel from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';
import Grid from '@material-ui/core/Grid';

const MyCarousel = () => (
    <Grid container spacing={4}>  
  <Carousel xs  plugins={['arrows']}>
      
    <img xs={12} sm={12} md={4} height="400px" width="1167" src="https://source.unsplash.com/user/erondu" />
    <img xs={3} sm={6} md={4}  height="400px" width="1167"src="https://source.unsplash.com/random" />
    <img xs={3} sm={6} md={4} height="400px" width="1167" src="https://www.fillmurray.com/640/360
" />  

  </Carousel>
  </Grid>
);
export default MyCarousel
